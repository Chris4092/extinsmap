package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;

public class PrietenieValidator implements Validator<Prietenie> {
    @Override
    public void validate(Prietenie entity) throws ValidationException {
        if(entity.getId().getLeft()<0 || entity.getId().getRight()<0)
            throw new ValidationException("Both ids must be positive numbers!");

    }
}
