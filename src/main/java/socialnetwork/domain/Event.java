package socialnetwork.domain;

import javafx.scene.image.ImageView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Event extends Entity<Long> {
    private String nume;
    private LocalDateTime data;
    private List<Long> participanti=new ArrayList<>();
    private String picturePath;
    private ImageView imageView;
    private String stringData;
    private String descriere;

    public Event(String nume, LocalDateTime data, List<Long> participanti, String picturePath, ImageView imageView,String descriere) {
        this.nume = nume;
        this.data = data;
        this.participanti = participanti;
        this.picturePath = picturePath;
        this.imageView = imageView;
        this.descriere = descriere;
        this.stringData=data.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }


    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public String getNume() {
        return nume;
    }

    public String getStringData() {
        return stringData;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public List<Long> getParticipanti() {
        return participanti;
    }

    public void setParticipanti(List<Long> participanti) {
        this.participanti = participanti;
    }

    public String getPicturePath() {
        return picturePath;
    }
    public void addParticipant(Long id){
        this.participanti.add(id);
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
}
