package socialnetwork.domain;

import java.time.LocalDateTime;

public class GrupMessage extends Entity<Long> {
    private Long from;
    private Long groupId;
    private String message;
    protected LocalDateTime date;

    public GrupMessage(Long from, Long groupId, String message, LocalDateTime date) {
        this.from = from;
        this.groupId = groupId;
        this.message = message;
        this.date = date;
    }

    public Long getFrom() {
        return from;
    }

    public Long getGroupId() {
        return groupId;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
