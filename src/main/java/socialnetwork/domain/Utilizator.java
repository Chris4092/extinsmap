package socialnetwork.domain;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utilizator extends Entity<Long>{
    private String firstName;
    private String lastName;
    private String picture;
    private ImageView imageView;
    private String username;
    private String hashing;
    private List<Long> friends = new ArrayList<Long>() {
   };
    boolean visited=false;

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Utilizator(String firstName, String lastName,String picture, ImageView imageView,String hashing,String username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture=picture;
        this.imageView = imageView;
        this.hashing=hashing;
        this.username=username;
    }
    public Utilizator(String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture="Pictures/noPicture.png";
        this.imageView = new ImageView(new Image(this.picture));
        imageView.setFitHeight(40);
        imageView.setFitWidth(40);
        this.username=username;
        this.hashing=String.valueOf(password.hashCode());
    }

    public Utilizator(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setFriends(List<Long> friends) {
        this.friends = friends;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashing() {
        return hashing;
    }

    public void setHashing(String hashing) {
        this.hashing = hashing;
    }



    public ImageView getImageView() {
        ImageView imageView = new ImageView(new Image(this.getPicture()));
        imageView.setFitHeight(40);
        imageView.setFitWidth(40);
        return  imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPicture() {
        return picture;
    }

    public void addFriend(Utilizator utilizator)
    {
        Long id = utilizator.getId();
        friends.add(id);
    }
    public void removeFriend(Long id)
    {
        friends.remove(id);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Long> getFriends() {
        return friends;
    }

    @Override
    public String toString() {
        return "Utilizator{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", friends=" + friends +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }
}