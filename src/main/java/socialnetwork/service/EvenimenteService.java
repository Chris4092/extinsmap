package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

public class EvenimenteService {
    private final Repository<Long, Utilizator> utilizatorRepo;
    private final Repository<Long, Event> eventRepo;

    public EvenimenteService(Repository<Long, Utilizator> utilizatorRepo, Repository<Long, Event> eventRepo) {
        this.utilizatorRepo = utilizatorRepo;
        this.eventRepo = eventRepo;
    }
   public Event addEvent(Event event){
        return this.eventRepo.save(event);
   }
   public Event deleteEvent(Long id){
        return this.eventRepo.delete(id);
   }
   public Event updateEvent(Event event){
        return this.eventRepo.update(event);
   }
    public Iterable<Event> getAll() {
        return eventRepo.findAll();
    }

    public Event findOne(Long id) {
        return eventRepo.findOne(id);
    }
}
