package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.PrietenieValidator;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.ServerException;
import java.rmi.server.ServerCloneException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public class PrietenieService extends Observable {
    private final Repository<Tuple<Long, Long>, Prietenie> prietenieRepo;
    private final Repository<Long, Utilizator> utilizatorRepo;

    /**
     * @param prietenieRepo  - Repository de prietenie
     * @param utilizatorRepo - Reposutitory de utilizator
     */

    public PrietenieService(Repository<Tuple<Long, Long>, Prietenie> prietenieRepo, Repository<Long, Utilizator> utilizatorRepo) {
        this.prietenieRepo = prietenieRepo;
        this.utilizatorRepo = utilizatorRepo;
    }


    /**
     * @param prietenie Un obiect de tip prietenie
     * @return un obiect de tip Prietenie daca a putut fi adaugat
     */
    public Prietenie addPrietenie(Prietenie prietenie) {


        Utilizator u1 = utilizatorRepo.findOne(prietenie.getId().getLeft());
        Utilizator u2 = utilizatorRepo.findOne(prietenie.getId().getRight());
        if (u1 != null && u2 != null) {
            Prietenie prt = prietenieRepo.save(prietenie);
            u1.addFriend(u2);
            u2.addFriend(u1);
            this.setChanged();
            this.notifyObservers();
            return prt;
        } else {
            throw new ServiceException("Both users must exist in order to form a friendship!");
        }

    }

    /**
     * @param id Id ul unei prietenii
     * @return Prietenia care a fost stearsa
     */
    public Prietenie deletePrietenie(Tuple<Long, Long> id) {
        Prietenie p = prietenieRepo.delete(id);
        Utilizator u1 = utilizatorRepo.findOne(p.getId().getLeft());
        Utilizator u2 = utilizatorRepo.findOne(p.getId().getRight());
        if (u1 != null && u2 != null) {
            u1.removeFriend(u2.getId());
            u2.removeFriend(u1.getId());
        } else {
            throw new ServiceException("Both users must exist in order to find the friendship");
        }
        this.setChanged();
        this.notifyObservers();
        return p;
    }


    public Prietenie updatePrietenie(Prietenie prietenie) {
        Prietenie prietenie1 = prietenieRepo.update(prietenie);
        this.setChanged();
        this.notifyObservers();
        return prietenie1;
    }

    /**
     * getter pentru prietenie
     *
     * @return O lista de tip Iterable cu toate prieteniile
     */
    public Iterable<Prietenie> getAll() {
        return prietenieRepo.findAll();
    }

    public ArrayList<String> toatePrieteniile(Long id) {
        Utilizator utilizator = utilizatorRepo.findOne(id);
        if (utilizator == null) {
            throw new ServiceException("The utilizator does not exist!");
        }
        ArrayList<Prietenie> toatePrieteniile = new ArrayList<Prietenie>();
        ArrayList<String> result = new ArrayList<>();
        this.prietenieRepo.findAll().forEach(toatePrieteniile::add);
        Stream<Prietenie> prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getId().getLeft().equals(id))
                .forEach(x -> {
                    String s = new String("");
                    s += this.utilizatorRepo.findOne(x.getId().getRight()).getLastName().toString();
                    s += "|";
                    s += this.utilizatorRepo.findOne(x.getId().getRight()).getFirstName().toString();
                    s += "|";
                    s += x.getDate();
                    result.add(s);
                });
        prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getId().getRight().equals(id))
                .forEach(x -> {
                    String s = new String("");
                    s += this.utilizatorRepo.findOne(x.getId().getLeft()).getLastName().toString();
                    s += "|";
                    s += this.utilizatorRepo.findOne(x.getId().getLeft()).getFirstName().toString();
                    s += "|";
                    s += x.getDate();
                    result.add(s);
                });
        return result;

    }

    public ArrayList<String> toatePrieteniileData(Long id, Month month) {
        Utilizator utilizator = utilizatorRepo.findOne(id);
        if (utilizator == null) {
            throw new ServiceException("The utilizator does not exist!");
        }
        ArrayList<Prietenie> toatePrieteniile = new ArrayList<Prietenie>();
        ArrayList<String> result = new ArrayList<>();
        this.prietenieRepo.findAll().forEach(toatePrieteniile::add);
        Stream<Prietenie> prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getDate().getMonth().equals(month))
                .filter(x -> x.getId().getLeft().equals(id))
                .forEach(x -> {
                    String s = new String("");
                    s += this.utilizatorRepo.findOne(x.getId().getRight()).getLastName().toString();
                    s += "|";
                    s += this.utilizatorRepo.findOne(x.getId().getRight()).getFirstName().toString();
                    s += "|";
                    s += x.getDate();
                    result.add(s);
                });
        prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getDate().getMonth().equals(month))
                .filter(x -> x.getId().getRight().equals(id))
                .forEach(x -> {
                    String s = new String("");
                    s += this.utilizatorRepo.findOne(x.getId().getLeft()).getLastName().toString();
                    s += "|";
                    s += this.utilizatorRepo.findOne(x.getId().getLeft()).getFirstName().toString();
                    s += "|";
                    s += x.getDate();
                    result.add(s);
                });
        return result;
    }

    public ArrayList<String> toatePrieteniileIntervalData(Long id, LocalDate dataInceput, LocalDate dataFinal) {
        Utilizator utilizator = utilizatorRepo.findOne(id);
        if (utilizator == null) {
            throw new ServiceException("The utilizator does not exist!");
        }
        ArrayList<Prietenie> toatePrieteniile = new ArrayList<Prietenie>();
        ArrayList<String> result = new ArrayList<>();
        this.prietenieRepo.findAll().forEach(toatePrieteniile::add);
        Stream<Prietenie> prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getDate().toLocalDate().isAfter(dataInceput))
                .filter(x -> x.getDate().toLocalDate().isBefore(dataFinal))
                .filter(x -> x.getId().getLeft().equals(id))
                .forEach(x -> {
                    String s = new String("");
                    s += this.utilizatorRepo.findOne(x.getId().getRight()).getLastName().toString();
                    s += "|";
                    s += this.utilizatorRepo.findOne(x.getId().getRight()).getFirstName().toString();
                    s += "|";
                    s += x.getDate();
                    result.add(s);
                });
        prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getDate().toLocalDate().isAfter(dataInceput))
                .filter(x -> x.getDate().toLocalDate().isBefore(dataFinal))
                .filter(x -> x.getId().getRight().equals(id))
                .forEach(x -> {
                    String s = new String("");
                    s += this.utilizatorRepo.findOne(x.getId().getLeft()).getLastName().toString();
                    s += "|";
                    s += this.utilizatorRepo.findOne(x.getId().getLeft()).getFirstName().toString();
                    s += "|";
                    s += x.getDate();
                    result.add(s);
                });
        return result;
    }

    public int numarPrieteniiIntervalData(Long id, LocalDate dataInceput, LocalDate dataFinal) {
        Utilizator utilizator = utilizatorRepo.findOne(id);
        if (utilizator == null) {
            throw new ServiceException("The utilizator does not exist!");
        }
        ArrayList<Prietenie> toatePrieteniile = new ArrayList<Prietenie>();
        AtomicReference<Integer> result = new AtomicReference<Integer>(0);
        this.prietenieRepo.findAll().forEach(toatePrieteniile::add);
        Stream<Prietenie> prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getDate().toLocalDate().isAfter(dataInceput))
                .filter(x -> x.getDate().toLocalDate().isBefore(dataFinal))
                .filter(x -> x.getId().getLeft().equals(id))
                .forEach(x -> {
                    result.set(result.get() + 1);
                });
        prietenieStream = toatePrieteniile.stream();
        prietenieStream
                .filter(x -> x.getDate().toLocalDate().isAfter(dataInceput))
                .filter(x -> x.getDate().toLocalDate().isBefore(dataFinal))
                .filter(x -> x.getId().getRight().equals(id))
                .forEach(x -> {

                    result.set(result.get() + 1);
                });
        return result.get();
    }

    public void incarcaPrietenieDacaExista(Utilizator logat, Utilizator testat) {
        Object u = null;
        try {
            Method method = this.prietenieRepo.getClass().getMethod("incarcaPrietenieDacaExista", Utilizator.class, Utilizator.class);
            u = method.invoke(this.prietenieRepo, logat, testat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public void incarcaToatePrieteniile(Utilizator logat) {
        Object u = null;
        try {
            Method method = this.prietenieRepo.getClass().getMethod("incarcaToatePrieteniile", Utilizator.class);
            u = method.invoke(this.prietenieRepo, logat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public void DOOM(){
        this.prietenieRepo.DOOM();
    }

}
