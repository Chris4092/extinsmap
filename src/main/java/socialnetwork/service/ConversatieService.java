package socialnetwork.service;

import socialnetwork.domain.Conversatie;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.AbstractDatabaseRepository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Observable;
import java.util.stream.Stream;

public class ConversatieService extends Observable {
    private AbstractDatabaseRepository<Long, Conversatie> conversatiiRepository;
    private Repository<Long, Utilizator> utilizatorRepository;

    public ConversatieService(AbstractDatabaseRepository<Long, Conversatie> messageRepository, Repository<Long, Utilizator> utilizatorRepository) {
        this.conversatiiRepository = messageRepository;
        this.utilizatorRepository = utilizatorRepository;
    }
    public Conversatie addConversatie(Conversatie conversatie){
        Conversatie c=this.conversatiiRepository.save(conversatie);
        this.setChanged();
        this.notifyObservers();
        return c;
    }
    public Iterable<Conversatie> getAll(){
        return this.conversatiiRepository.findAll();
    }
    public ArrayList<Conversatie> toateMesajele(Long user1,Long user2){
        ArrayList<Conversatie>result =new ArrayList<>();
        this.conversatiiRepository.findAll().forEach(x->{
            if((x.getTo().equals(user1)&&x.getFrom().equals(user2))||(x.getTo().equals(user2)&&x.getFrom().equals(user1)))
            {
                result.add(x);
            }
        });
        Comparator<Conversatie>comparator= Comparator.comparing(Conversatie::getDate);
        result.sort(comparator);
        return result;
    }

    public ArrayList<Conversatie> toateMesajeleIntervalData(Long id, LocalDate dataInceput,LocalDate dataFinal){
        ArrayList<Conversatie> toateConversatiile = new ArrayList<Conversatie>();
        ArrayList<Conversatie> result = new ArrayList<>();
        this.conversatiiRepository.findAll().forEach(toateConversatiile::add);
        Stream<Conversatie> conversatieStream = toateConversatiile.stream();
        conversatieStream
                .filter(x -> x.getDate().toLocalDate().isAfter(dataInceput))
                .filter(x -> x.getDate().toLocalDate().isBefore(dataFinal))
                .filter(x -> x.getTo().equals(id))
                .forEach(result::add);
        return result;
    }
    public void DOOM(){
        this.conversatiiRepository.DOOM();
    }
    public void incarcaConversatieDacaExista(Utilizator logat,Utilizator testat) {
        Object u = null;
        try {
            Method method = this.conversatiiRepository.getClass().getMethod("incarcaConversatieDacaExista", Utilizator.class, Utilizator.class);
            u = method.invoke(this.conversatiiRepository, logat, testat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void incarcaToateConversatiile(Utilizator logat){
        Object u = null;
        try {
            Method method = this.conversatiiRepository.getClass().getMethod("incarcaToateConversatiile", Utilizator.class);
            u = method.invoke(this.conversatiiRepository, logat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
