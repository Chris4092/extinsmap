package socialnetwork.service;

import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.AbstractDatabaseRepository;


import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicReference;

public class MessageService extends Observable{
    private AbstractDatabaseRepository<Long, Message> messageRepository;
    private Repository<Long, Utilizator> utilizatorRepository;

    public MessageService(AbstractDatabaseRepository<Long, Message> messageRepository, Repository<Long, Utilizator> utilizatorRepository) {
        this.messageRepository = messageRepository;
        this.utilizatorRepository = utilizatorRepository;
    }

    public Message addMessage(Message message){
        Message E=this.messageRepository.save(message);

        if(message.getRepliedMessage()!=null){
            Long i=message.getRepliedMessage();
            if(this.messageRepository.findOne(i)==null){
                throw new ServiceException("There is no message with the given id!\n");
            }
            Message mesajRaspuns=this.messageRepository.findOne(message.getRepliedMessage());
            mesajRaspuns.setReply(message.getId());
            this.messageRepository.reloadDataBase();
        }
        this.setChanged();
        this.notifyObservers();
        return E;
    }
    public Iterable<Message> getAll(){
        return messageRepository.findAll();
    }

    public Message findOne(Long id){
        return this.messageRepository.findOne(id);
    }
    public ArrayList<Message>mesajeLaCareNuADatReply(Long idUtilizator){
        ArrayList<Message>result=new ArrayList<>();
        this.messageRepository.findAll().forEach(x->{
            if(x.getReply()==null){
                if(x.getTo().size() == 1) {
                    x.getTo().forEach(y -> {
                        if (y.equals(idUtilizator)) {
                            result.add(x);
                        }
                    });
                }
            }
        });
        return result;
    }
    public ArrayList<Message>mesajeLaCareNuADatReplyGrup(Long idUtilizator){
        ArrayList<Message>result=new ArrayList<>();
        this.messageRepository.findAll().forEach(x->{
            if(x.getReply()==null){
                if(x.getTo().size() > 1) {
                    x.getTo().forEach(y -> {
                        if (y.equals(idUtilizator)) {
                            result.add(x);
                        }
                    });
                }
            }
        });
        return result;
    }
    public ArrayList<ArrayList<Message>> toateConversatiile(Long idUtilizator1,Long idUtilizator2){
        if(this.utilizatorRepository.findOne(idUtilizator2)==null){
            throw new ServiceException("The user does not exist!\n");
        }
        ArrayList<ArrayList<Message>> result=new ArrayList<>();
        this.messageRepository.findAll().forEach(x->{
            if(x.getFrom().equals(idUtilizator1)&&x.getReply()==null){
                ArrayList<Message>conv=new ArrayList<>();

                if(x.getTo().size() == 1) {
                    x.getTo().forEach(y -> {
                        if (y.equals(idUtilizator2)) {
                            Message msg = x;
                            while (msg.getRepliedMessage() != null) {
                                conv.add(msg);
                                msg = this.messageRepository.findOne(msg.getRepliedMessage());
                            }
                            conv.add(msg);
                        }
                    });
                }
                result.add(conv);

            }
        });
        this.messageRepository.findAll().forEach(x->{
            if(x.getFrom().equals(idUtilizator2)&&x.getReply()==null){
                ArrayList<Message>conv=new ArrayList<>();

                if(x.getTo().size() == 1) {
                    x.getTo().forEach(y -> {
                        if (y.equals(idUtilizator1)) {
                            Message msg = x;
                            while (msg.getRepliedMessage() != null) {
                                conv.add(msg);
                                msg = this.messageRepository.findOne(msg.getRepliedMessage());
                            }
                            conv.add(msg);
                        }
                    });
                }
                result.add(conv);

            }
        });
        return result;

    }

    public ArrayList<ArrayList<Message>> toateConversatiileGrup(Long idUtilizator1){
        ArrayList<ArrayList<Message>> result=new ArrayList<>();
        this.messageRepository.findAll().forEach(x->{
            if(x.getFrom().equals(idUtilizator1)&&x.getReply()==null){
                ArrayList<Message>conv=new ArrayList<>();
                if(x.getTo().size() > 1) {

                        Message msg = x;
                        while (msg.getRepliedMessage() != null) {
                            conv.add(msg);
                            msg = this.messageRepository.findOne(msg.getRepliedMessage());
                        }
                        conv.add(msg);
                }
                if(conv.size() > 0)
                    result.add(conv);

            }
        });
        this.messageRepository.findAll().forEach(x->{
            if(x.getReply()==null){
                ArrayList<Message>conv=new ArrayList<>();
                if(x.getTo().size() > 1) {
                    x.getTo().forEach(y -> {
                        if (y.equals(idUtilizator1)) {
                            Message msg = x;
                            while (msg.getRepliedMessage() != null) {
                                conv.add(msg);
                                msg = this.messageRepository.findOne(msg.getRepliedMessage());
                            }
                            conv.add(msg);
                        }
                    });
                }
                if(conv.size() > 0)
                    result.add(conv);

            }
        });
        return result;
    }

    public boolean existaMesaj(ArrayList<Message> mesaje, Long id)
    {
        AtomicReference<Boolean> exista=new AtomicReference<>(false);
        mesaje.forEach(x->{
            if(x.getId().equals(id))
                exista.set(true);
        });
        return exista.get();
    }


}
