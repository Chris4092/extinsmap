package socialnetwork.service;

import socialnetwork.domain.Conversatie;
import socialnetwork.domain.Grup;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.AbstractDatabaseRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;

public class GrupService extends Observable {
    private AbstractDatabaseRepository<Long, Grup> grupRepository;
    private Repository<Long, Utilizator> utilizatorRepository;

    public GrupService(AbstractDatabaseRepository<Long, Grup> grupRepository, Repository<Long, Utilizator> utilizatorRepository) {
        this.grupRepository = grupRepository;
        this.utilizatorRepository = utilizatorRepository;
    }
    public Grup addGrup(Grup conversatie){
        Grup c=this.grupRepository.save(conversatie);
        //this.setChanged();
        //this.notifyObservers();
        return c;
    }
    public Iterable<Grup>getAll(){return this.grupRepository.findAll();}

    public List<Grup> toateGrupurileUtilizator(Long idUtilizator){
        List<Grup> result=new ArrayList<>();
        this.grupRepository.findAll().forEach(x->{
            if(x.getMembers().contains(idUtilizator))
                result.add(x);
        });

        return result;
    }

    public Grup findOne(Long id){
        return  this.grupRepository.findOne(id);
    }

}
