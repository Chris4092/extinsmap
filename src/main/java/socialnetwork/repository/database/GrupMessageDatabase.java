package socialnetwork.repository.database;

import socialnetwork.domain.GrupMessage;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;

public class GrupMessageDatabase extends AbstractDatabaseRepository<Long, GrupMessage> {
    public GrupMessageDatabase(Validator<GrupMessage> validator, String url) {
        super(validator, url);
        this.loadData();
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].MesajeDeGrup");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    GrupMessage message = this.extractEntity(resultSet);
                    super.superSave(message);
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    @Override
    public GrupMessage extractEntity(ResultSet resultSet) {
        try{
            Long id=resultSet.getLong("idMesaj");
            Long idGrup=resultSet.getLong("idGrup");
            Long from=resultSet.getLong("fromMesaj");
            String mesaj=resultSet.getString("mesaj");
            String date=resultSet.getString("dataMesaj");
            LocalDateTime mesajData=LocalDateTime.parse(date);
            GrupMessage grupMessage=new GrupMessage(from,idGrup,mesaj,mesajData);
            grupMessage.setId(id);
            return  grupMessage;
        }
        catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    @Override
    public void writeToDatabase(GrupMessage grupMessage) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO [socialNetwork].MesajeDeGrup"+
                    " VALUES ("+grupMessage.getGroupId()+",'"+grupMessage.getMessage()+"','"+
                    grupMessage.getDate().toString()+"',"+grupMessage.getFrom()+")",Statement.RETURN_GENERATED_KEYS);
            ResultSet keys=statement.getGeneratedKeys();
            Long key=null;
            if(keys.next()){
                key=keys.getLong(1);
            }
            grupMessage.setId(key);

        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not write to table!");
        }

    }

    @Override
    public void emptyTable() {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].MesajeDeGrup");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }

    }

    @Override
    public void deleteEntity(GrupMessage message) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].MesajeDeGrup WHERE idMesaj = "+message.getId()+";");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void updateEntity(GrupMessage message) {

    }
}
