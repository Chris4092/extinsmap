package socialnetwork.repository.database;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;

public class CereriPrietenieDatabase extends AbstractDatabaseRepository<Tuple<Long,Long>, CererePrietenie>{
    public CereriPrietenieDatabase(Validator<CererePrietenie> validator, String url) {
        super(validator, url);
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].CereriPrietenie");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    CererePrietenie cererePrietenie = this.extractEntity(resultSet);
                    super.superSave(cererePrietenie);
                }
            }
            connection.close();
        }
        catch (SQLException exception){
            throw new RepoException("The database connection failed");
        }
    }

    @Override
    public CererePrietenie extractEntity(ResultSet resultSet) {
        try{
            Long id1 = resultSet.getLong("idUtilizatorT");
            Long id2 = resultSet.getLong("idUtilizatorP");
            String statusString = resultSet.getString("statusCerere");
            Status status = Status.PENDING;
            if(statusString.equals("pending"))
            {
                status = Status.PENDING;
            }
            else if(statusString.equals("approved"))
            {
                status = Status.APPROVED;
            }
            else if(statusString.equals("rejected"))
            {
                status = Status.REJECTED;
            }
            CererePrietenie cererePrietenie = new CererePrietenie(status);
            cererePrietenie.setId(new Tuple<>(id1,id2));
            return cererePrietenie;
        }
        catch (SQLException exception) {
            throw new RepoException("Could not extract entity from table. Check if the fields' values are valid!");
        }
    }

    @Override
    public void writeToDatabase(CererePrietenie cererePrietenie) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            Status statusel = cererePrietenie.getStatus();
            String status = "";
            if(statusel == Status.PENDING)
            {
                status = "pending";
            }
            else if(statusel == Status.APPROVED)
            {
                status = "approved";
            }
            else if(statusel == Status.REJECTED)
            {
                status = "rejected";
            }

            statement.executeUpdate("INSERT INTO [socialNetwork].CereriPrietenie(idUtilizatorT,idUtilizatorP,statusCerere)" +
                    " VALUES (" + cererePrietenie.getId().getLeft().toString() + "," + cererePrietenie.getId().getRight().toString() +
                    ",'" + status + "')");
            connection.close();
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not write to table!");
        }

    }

    @Override
    public void emptyTable() {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [socialNetwork].CereriPrietenie");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void deleteEntity(CererePrietenie cererePrietenie) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String query="DELETE FROM [socialNetwork].CereriPrietenie WHERE " +
                    "idUtilizatorT = "+cererePrietenie.getId().getLeft()+
                    " AND idUtilizatorP = "+cererePrietenie.getId().getRight()+";";
            statement.executeUpdate(query);
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void updateEntity(CererePrietenie cererePrietenie) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String query="UPDATE [socialNetwork].CereriPrietenie " +
                    "SET " +
                    "statusCerere = '"+cererePrietenie.getStatus().toString().toLowerCase()+"', " +
                    "idUtilizatorT = "+cererePrietenie.getId().getLeft()+", "+
                    "idUtilizatorP = "+cererePrietenie.getId().getRight()+
                    " WHERE " +
                    "idUtilizatorT = "+cererePrietenie.getId().getLeft()+
                    " AND idUtilizatorP = "+cererePrietenie.getId().getRight()+";";
            statement.executeUpdate(query);
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    public void incarcaCerereDacaExista(Utilizator logat, Utilizator testat){
        try {
            Connection connection = DriverManager.getConnection(url);
            String query="select * from [socialNetwork].CereriPrietenie " +
                    "where " +
                    "(idUtilizatorT = "+logat.getId()+" and idUtilizatorP = "+testat.getId()+" ) " +
                    "or " +
                    "(idUtilizatorT = "+testat.getId()+" and idUtilizatorP = "+logat.getId()+")";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    CererePrietenie prietenie = this.extractEntity(resultSet);
                    if(!this.entities.containsKey(prietenie.getId())) {
                        super.superSave(prietenie);
                    }
                }
            }
            connection.close();
        }
        catch (SQLException exception){
            throw new RepoException("The database connection failed");
        }
    }
}
