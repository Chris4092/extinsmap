package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.file.AbstractFileRepository;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class PrieteniiDatabase extends AbstractDatabaseRepository<Tuple<Long,Long>, Prietenie> {
    public PrieteniiDatabase(Validator<Prietenie> validator, String url) {
        super(validator, url);
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Prietenii");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Prietenie prietenie = this.extractEntity(resultSet);
                    super.superSave(prietenie);
                }
            }
            connection.close();
        }
        catch (SQLException exception){
            throw new RepoException("The database connection failed");
        }
    }

    @Override
    public Prietenie extractEntity(ResultSet resultSet) {
        try{
            Long id1 = resultSet.getLong("idUtilizator1");
            Long id2 = resultSet.getLong("idUtilizator2");
            String date = resultSet.getString("dataImprietenire");
            LocalDateTime localDateTime = LocalDateTime.parse(date);
            Prietenie prietenie = new Prietenie(localDateTime);
            prietenie.setId(new Tuple<>(id1,id2));
            return prietenie;
        }
        catch (SQLException exception) {
            throw new RepoException("Could not extract entity from table. Check if the fields' values are valid!");
        }
    }

    @Override
    public void writeToDatabase(Prietenie prietenie) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO [socialNetwork].Prietenii(idUtilizator1,idUtilizator2,dataImprietenire)" +
                    " VALUES (" + prietenie.getId().getLeft().toString() + "," + prietenie.getId().getRight().toString() +
                    ",'" + prietenie.getDate().toString() + "')");
            connection.close();
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not write to table!");
        }
    }

    @Override
    public void emptyTable() {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [socialNetwork].Prietenii");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void deleteEntity(Prietenie prietenie) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String query="DELETE FROM [socialNetwork].Prietenii WHERE " +
                    "idUtilizator1 = "+prietenie.getId().getLeft()+
                    " AND idUtilizator2 = "+prietenie.getId().getRight()+";";

            statement.executeUpdate(query);
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    public void incarcaPrietenieDacaExista(Utilizator logat,Utilizator testat){
        try {
            Connection connection = DriverManager.getConnection(url);
            String query="select * from [socialNetwork].Prietenii " +
                    "where " +
                    "(idUtilizator1 = "+logat.getId()+" and idUtilizator2 = "+testat.getId()+" ) " +
                    "or " +
                    "(idUtilizator1 = "+testat.getId()+" and idUtilizator2 = "+logat.getId()+")";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Prietenie prietenie = this.extractEntity(resultSet);
                    if(!this.entities.containsKey(prietenie.getId())) {
                        super.superSave(prietenie);
                    }
                }
            }
            connection.close();
        }
        catch (SQLException exception){
            throw new RepoException("The database connection failed");
        }
    }

    public void incarcaToatePrieteniile(Utilizator logat){
        try {
            Connection connection = DriverManager.getConnection(url);
            String query="select * from [socialNetwork].Prietenii " +
                    "where " +
                    "(idUtilizator1 = "+logat.getId()+
                    " or " +
                    "idUtilizator2 = "+logat.getId()+" )";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            {
                while (resultSet.next()) {
                    Prietenie prietenie = this.extractEntity(resultSet);
                    if(!this.entities.containsKey(prietenie.getId())) {
                        super.superSave(prietenie);
                    }
                }
            }
            connection.close();
        }
        catch (SQLException exception){
            throw new RepoException("The database connection failed");
        }
    }

    @Override
    public void updateEntity(Prietenie prietenie) {

    }
}
