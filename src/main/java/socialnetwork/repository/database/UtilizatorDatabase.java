package socialnetwork.repository.database;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.file.AbstractFileRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class UtilizatorDatabase extends AbstractDatabaseRepository<Long, Utilizator> {
    public UtilizatorDatabase(Validator<Utilizator> validator, String url) {
        super(validator, url);
        //this.incarcaPrimii(this.findOneDataBase("Chris4092"));
        //this.incarcaNext(this.findOne(Long.parseLong("21")),this.findOneDataBase("Chris4092"));
        //this.incarcaPrev(this.findOne(Long.parseLong("22")),this.findOneDataBase("Chris4092"));
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Utilizatori");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Utilizator utilizator = this.extractEntity(resultSet);
                    super.superSave(utilizator);
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }

    }

    @Override
    public Utilizator extractEntity(ResultSet resultSet) {
        try {
            Long id = resultSet.getLong("idUtilizator");
            //vezi si tu nstring
            String nume = resultSet.getString("numeUtilizator");
            String prenume = resultSet.getString("prenumeUtilizator");
            String poza = resultSet.getString("pozaUtilizator");
            String username = resultSet.getString("usernameUtilizator");
            String hashing = resultSet.getString("hashingUtilizator");
            ImageView imageView = new ImageView(new Image(poza));
            imageView.setFitHeight(40);
            imageView.setFitWidth(40);
            Utilizator utilizator = new Utilizator(prenume, nume, poza, imageView, hashing, username);
            utilizator.setId(id);
            return utilizator;
        } catch (SQLException exception) {
            throw new RepoException("Could not extract entity from table. Check if the fields' values are valid!");
        }
    }

    @Override
    public void writeToDatabase(Utilizator utilizator) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO [socialNetwork].Utilizatori" +
                    " VALUES (" + "'" + utilizator.getLastName() + "','"
                    + utilizator.getFirstName() + "','" + utilizator.getPicture() + "','" + utilizator.getUsername()
                    + "','" + utilizator.getHashing() + "')", Statement.RETURN_GENERATED_KEYS);
            ResultSet keys = statement.getGeneratedKeys();
            Long key = null;
            if (keys.next()) {
                key = keys.getLong(1);
            }
            utilizator.setId(key);
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("Could not write to table!");
        }
    }

    @Override
    public void emptyTable() {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [socialNetwork].Utilizatori");
        } catch (SQLException exception) {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void deleteEntity(Utilizator E) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [socialNetwork].Utilizatori U WHERE U.idUtilizator = " + E.getId() + ";");
        } catch (SQLException exception) {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void updateEntity(Utilizator utilizator) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String query = "UPDATE [socialNetwork].Utilizatori " +
                    "SET " +
                    "numeUtilizator = '" + utilizator.getFirstName() + "', " +
                    "prenumeUtilizator = '" + utilizator.getLastName() + "', " +
                    "usernameUtilizator = '" + utilizator.getUsername() + "', " +
                    "hashingUtilizator = '" + utilizator.getHashing() + "', " +
                    "pozaUtilizator = '" + utilizator.getPicture() + "'" +
                    " WHERE idUtilizator = " + utilizator.getId() + ";";
            statement.executeUpdate(query);
        } catch (SQLException exception) {
            throw new RepoException("Could not empty table");
        }
    }

    public Utilizator findOneDataBase(String username) {
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "SELECT * FROM [socialNetwork].Utilizatori U WHERE U.usernameUtilizator = '" + username + "'";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            Utilizator u = null;
            while (resultSet.next()) {
                u = this.extractEntity(resultSet);
            }
            return u;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    public Utilizator findOneDataBaseId(Long id) {
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "SELECT * FROM [socialNetwork].Utilizatori U WHERE U.idUtilizator = " + id;
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            Utilizator u = null;
            while (resultSet.next()) {
                u = this.extractEntity(resultSet);
            }
            return u;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * Functia incarca urmatorii 6 utilizatori in pagina (6 utilizatori dupa cel primit in parametrii) (Incluzand utilizatorul logat)
     *
     * @param lastOne ultimul utilizator afisat in lista
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaNext(Utilizator lastOne, Utilizator curent) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT TOP 12 * FROM [socialNetwork].Utilizatori WHERE idUtilizator != " + curent.getId() +
                    " AND idUtilizator >" + lastOne.getId()
                    + " order by idUtilizator");
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            this.entities.put(curent.getId(), curent);
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 11) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 12;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * Functia incarca primii 6 utilizaotri in pagina(Incluzand utilizatorul logat)
     *
     * @param curent utilizaotrul curent
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaPrimii(Utilizator curent) {

        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT TOP 12 * FROM [socialNetwork].Utilizatori WHERE idUtilizator != " + curent.getId()
                    + " order by idUtilizator");
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            this.entities.put(curent.getId(), curent);
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 11) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 12;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * @param lastOne
     * @param curent
     * @return Raspunde la intrebarea "Mai exista utilizatori pe pagina anterioara?"
     */
    public boolean incarcaPrev(Utilizator lastOne, Utilizator curent) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT TOP 12 * FROM [socialNetwork].Utilizatori WHERE idUtilizator != " + curent.getId() +
                    " AND idUtilizator <" + lastOne.getId()
                    + " order by idUtilizator desc");
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            this.entities.put(curent.getId(), curent);
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 11) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        all.add(utilizator);
                    }
                    n++;
                }
            }
            for (int i = all.size() - 1; i >= 0; i--) {
                this.entities.put(all.get(i).getId(), all.get(i));
            }
            connection.close();
            return n == 12;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * Functia incarca primii 6 prieteni ai utilizatorului logat
     *
     * @param logat - Utilizatorul Logat
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaPrimiiPrieteni(Utilizator logat) {

        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 13 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].Prietenii P on " +
                    "(P.idUtilizator1 = U.idUtilizator and p.idUtilizator2= " + logat.getId() + ") or " +
                    "(P.idUtilizator2=U.idUtilizator and P.idUtilizator1= " + logat.getId() + " ) " +
                    "order by U.idUtilizator";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 12) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 13;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * Functia incarca urmatorii 6 prieteni ai utilizatorului logat
     *
     * @param logat   - Utilizatorul Logat
     * @param lastOne - Ultimul utizilator de pe lista
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaUrmatoriiPrieteni(Utilizator logat, Utilizator lastOne) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 13 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].Prietenii P on " +
                    "(P.idUtilizator1 = U.idUtilizator and p.idUtilizator2= " + logat.getId() + ") or" +
                    "(P.idUtilizator2=U.idUtilizator and P.idUtilizator1= " + logat.getId() + " ) " +
                    " where idUtilizator >" + lastOne.getId()
                    + " order by idUtilizator";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 12) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 13;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }


    /**
     * Functia incarca anteriorii 6 prieteni ai utilizatorului logat
     *
     * @param logat   - Utilizatorul Logat
     * @param lastOne - Ultimul utizilator de pe lista
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaAnterioriiPrieteni(Utilizator logat, Utilizator lastOne) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 13 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].Prietenii P on " +
                    "(P.idUtilizator1 = U.idUtilizator and p.idUtilizator2= " + logat.getId() + ") or" +
                    "(P.idUtilizator2=U.idUtilizator and P.idUtilizator1= " + logat.getId() + " ) " +
                    " where idUtilizator <" + lastOne.getId()
                    + " order by idUtilizator desc";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 12) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        all.add(utilizator);
                    }
                    n++;
                }
            }
            for (int i = all.size() - 1; i >= 0; i--) {
                this.entities.put(all.get(i).getId(), all.get(i));
            }
            connection.close();
            return n == 13;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    public boolean incarcaPrimiiPrieteniMess(Utilizator logat) {

        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 7 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].Prietenii P on " +
                    "(P.idUtilizator1 = U.idUtilizator and p.idUtilizator2= " + logat.getId() + ") or " +
                    "(P.idUtilizator2=U.idUtilizator and P.idUtilizator1= " + logat.getId() + " ) " +
                    "order by U.idUtilizator";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 6) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 7;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * Functia incarca urmatorii 6 prieteni ai utilizatorului logat
     *
     * @param logat   - Utilizatorul Logat
     * @param lastOne - Ultimul utizilator de pe lista
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaUrmatoriiPrieteniMess(Utilizator logat, Utilizator lastOne) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 7 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].Prietenii P on " +
                    "(P.idUtilizator1 = U.idUtilizator and p.idUtilizator2= " + logat.getId() + ") or" +
                    "(P.idUtilizator2=U.idUtilizator and P.idUtilizator1= " + logat.getId() + " ) " +
                    " where idUtilizator >" + lastOne.getId()
                    + " order by idUtilizator";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 6) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 7;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }


    /**
     * Functia incarca anteriorii 6 prieteni ai utilizatorului logat
     *
     * @param logat   - Utilizatorul Logat
     * @param lastOne - Ultimul utizilator de pe lista
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaAnterioriiPrieteniMess(Utilizator logat, Utilizator lastOne) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 7 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].Prietenii P on " +
                    "(P.idUtilizator1 = U.idUtilizator and p.idUtilizator2= " + logat.getId() + ") or" +
                    "(P.idUtilizator2=U.idUtilizator and P.idUtilizator1= " + logat.getId() + " ) " +
                    " where idUtilizator <" + lastOne.getId()
                    + " order by idUtilizator desc";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 6) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        all.add(utilizator);
                    }
                    n++;
                }
            }
            for (int i = all.size() - 1; i >= 0; i--) {
                this.entities.put(all.get(i).getId(), all.get(i));
            }
            connection.close();
            return n == 7;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    public boolean incarcaPrimiiCereri(Utilizator logat) {

        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 13 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].CereriPrietenie CP on " +
                    "(CP.idUtilizatorT = U.idUtilizator and CP.idUtilizatorP= " + logat.getId() + " ) " +
                    "WHERE CP.statusCerere='pending' " +
                    "order by U.idUtilizator";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 12) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 13;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    public boolean incarcaUrmatoriiCereri(Utilizator logat, Utilizator lastOne) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 13 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].CereriPrietenie CP on " +
                    "(CP.idUtilizatorT = U.idUtilizator and CP.idUtilizatorP= " + logat.getId() + " ) " +
                    "WHERE CP.statusCerere='pending' " +
                    " and U.idUtilizator >" + lastOne.getId()
                    + " order by U.idUtilizator";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 12) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        this.entities.put(utilizator.getId(), utilizator);
                    }
                    n++;
                }
            }
            connection.close();
            return n == 13;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    public boolean incarcaAnterioriiCereri(Utilizator logat, Utilizator lastOne) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select top 13 U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].CereriPrietenie CP on " +
                    "(CP.idUtilizatorT = U.idUtilizator and CP.idUtilizatorP= " + logat.getId() + " ) " +
                    "WHERE CP.statusCerere='pending' " +
                    " AND U.idUtilizator <" + lastOne.getId()
                    + " order by U.idUtilizator desc";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            int n = 0;
            {
                while (resultSet.next()) {
                    if (n < 12) {
                        Utilizator utilizator = this.extractEntity(resultSet);
                        all.add(utilizator);
                    }
                    n++;
                }
            }
            for (int i = all.size() - 1; i >= 0; i--) {
                this.entities.put(all.get(i).getId(), all.get(i));
            }
            connection.close();
            return n == 13;
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * Functia incarca urmatorii 6 utilizatori in pagina (6 utilizatori dupa cel primit in parametrii) (Incluzand utilizatorul logat)
     *
     * @param lastOne ultimul utilizator afisat in lista
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaNextFiltrati(Utilizator lastOne, Utilizator curent, String filtr) {
        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Utilizatori");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Utilizator utilizator = this.extractEntity(resultSet);
                    all.add(utilizator);
                }
            }

            connection.close();
            all = all.stream()
                    .filter(x -> {
                        String s = x.getFirstName() + " " + x.getLastName();
                        String s2 = x.getLastName() + " " + x.getFirstName();
                        return s.startsWith(filtr) || s2.startsWith(filtr);
                    })
                    .collect(Collectors.toList());
            this.entities.clear();
            this.entities.put(curent.getId(), curent);
            AtomicReference<Boolean> ok = new AtomicReference<>(false);
            AtomicReference<Integer> contor = new AtomicReference<>(0);
            AtomicReference<Long> lastId = new AtomicReference<>();
            all.forEach(x -> {
                if (ok.get() && contor.get() < 12) {
                    this.entities.put(x.getId(), x);
                    if (x.getId().equals(curent.getId())) {
                        contor.set(contor.get() - 1);
                    }
                    contor.set(contor.get() + 1);
                    lastId.set(x.getId());
                }
                if (x.getId().equals(lastOne.getId())) {
                    ok.set(true);
                }
            });
            return !all.get(all.size() - 1).getId().equals(lastId.get());
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }

    }

    /**
     * Functia incarca primii 6 utilizaotri in pagina(Incluzand utilizatorul logat)
     *
     * @param curent utilizaotrul curent
     * @return Raspunde la intrebarea "Mai exista utilizatori pe urmatoarea pagina?"
     */
    public boolean incarcaPrimiiFiltrati(Utilizator curent, String filtr) {

        List<Utilizator> all = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Utilizatori");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Utilizator utilizator = this.extractEntity(resultSet);
                    all.add(utilizator);
                }
            }
            connection.close();

            all = all.stream()
                    .filter(x -> {
                        String s = x.getFirstName() + " " + x.getLastName();
                        String s2 = x.getLastName() + " " + x.getFirstName();
                        return s.startsWith(filtr) || s2.startsWith(filtr);
                    })
                    .collect(Collectors.toList());
            this.entities.clear();
            this.entities.put(curent.getId(), curent);
            int n = 12;
            Long lastId = Long.parseLong("0");
            for (int i = 0; i < n; i++) {
                if (i > all.size() - 1) {
                    break;
                }
                this.entities.put(all.get(i).getId(), all.get(i));
                if (all.get(i).getId().equals(curent.getId())) {
                    n++;
                }
                lastId = all.get(i).getId();
            }
            return !all.get(all.size() - 1).getId().equals(lastId);
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    /**
     * @param lastOne
     * @param curent
     * @return Raspunde la intrebarea "Mai exista utilizatori pe pagina anterioara?"
     */
    public boolean incarcaPrevFiltrati(Utilizator lastOne, Utilizator curent, String filtr) {
        List<Utilizator> all = new ArrayList<>();
        try {
            all.add(curent);
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Utilizatori");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Utilizator utilizator = this.extractEntity(resultSet);
                    if (!utilizator.getId().equals(curent.getId())) {
                        all.add(utilizator);
                    }
                }
            }
            connection.close();
            all = all.stream()
                    .filter(x -> {
                        String s = x.getFirstName() + " " + x.getLastName();
                        String s2 = x.getLastName() + " " + x.getFirstName();
                        return s.startsWith(filtr) || s2.startsWith(filtr);
                    })
                    .collect(Collectors.toList());

            this.entities.clear();
            this.entities.put(curent.getId(), curent);
            boolean ok = false;
            int counter = 0;
            Long lastId = Long.parseLong("0");
            List<Utilizator> ideeaLui = new ArrayList<>();
            for (int i = all.size() - 1; i >= 0; i--) {
                if (ok && counter < 12) {
                    if (!all.get(i).getId().equals(curent.getId())) {
                        counter++;
                        // this.entities.put(all.get(i).getId(), all.get(i));
                        ideeaLui.add(all.get(i));
                        lastId = all.get(i).getId();
                    }
                }
                if (all.get(i).getId().equals(lastOne.getId())) {
                    ok = true;
                }
            }
            for (int i = ideeaLui.size() - 1; i >= 0; i--) {
                this.entities.put(ideeaLui.get(i).getId(), ideeaLui.get(i));
            }
            lastId = ideeaLui.get(ideeaLui.size() - 1).getId();
            return !all.get(0).getId().equals(lastId);

        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }


    public void bagaTotiPrietenii(Utilizator logat) {
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select U.idUtilizator,U.numeUtilizator,U.prenumeUtilizator,U.pozaUtilizator,U.hashingUtilizator,U.usernameUtilizator from [socialNetwork].Utilizatori U " +
                    "inner join [socialNetwork].Prietenii P on " +
                    "(P.idUtilizator1 = U.idUtilizator and p.idUtilizator2= " + logat.getId() + ") or " +
                    "(P.idUtilizator2=U.idUtilizator and P.idUtilizator1= " + logat.getId() + " )";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            {
                while (resultSet.next()) {
                    Utilizator utilizator = this.extractEntity(resultSet);
                    super.superSave(utilizator);
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }

    }
}



