package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PrieteniiFile extends AbstractFileRepository<Tuple<Long,Long>, Prietenie> {
    public PrieteniiFile(String fileName, Validator<Prietenie> validator) {
        super(fileName, validator);
    }

    @Override
    public Prietenie extractEntity(List<String> attributes) {
        try {
            Prietenie prietenie = new Prietenie(LocalDateTime.parse(attributes.get(2)));
            prietenie.setId(new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1))));
            return prietenie;
        }
        catch (Exception e) {
            throw new RepoException("Invalid repository! Try again later");
        }
    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId().getLeft()+";"+entity.getId().getRight() + ";" +entity.getDate();
    }
}
