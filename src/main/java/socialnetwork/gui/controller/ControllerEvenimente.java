package socialnetwork.gui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;
import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EvenimenteService;
import socialnetwork.service.UtilizatorService;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ControllerEvenimente {
    private EvenimenteService evenimenteService;
    private UtilizatorService utilizatorService;
    Long idUtilizator;
    Long lastIdEventClicked;
    @FXML private TableView<Event>idEventTable;
    @FXML private TableColumn<Event, ImageView>idPictureEvent;
    @FXML private TableColumn<Event, String>idNameEvent;
    @FXML private TableColumn<Event, String>idDataEvent;
    @FXML private Button myEventsButtonId;
    @FXML private Button allEventsButtonId;
    @FXML private Button idSubscribeButton;
    @FXML private Button idUnsubscribeButton;
    @FXML private ImageView idPozaEvent;
    @FXML private Label idNumeEventLabel;
    @FXML private Label idDataEventLabel;
    @FXML private Label idLabel1;
    @FXML private Label idLabel2;
    @FXML private Label idLabel3;
    @FXML private Label idDescriereLabel;
    private Queue<String> notificariEvenimente = new LinkedBlockingQueue<>();
    private ObservableList<Event> model = FXCollections.observableArrayList();
    public void init(UtilizatorService utilizatorService,EvenimenteService evenimenteService,Long idUtilizator) {
        this.evenimenteService = evenimenteService;
        this.utilizatorService = utilizatorService;
        this.idUtilizator=idUtilizator;
        this.evenimenteService.getAll().forEach(x -> {
            if (x.getParticipanti().contains(idUtilizator)) {
                String s = "";
                s+= "Evenimentul " + x.getNume() + " va avea loc in " + Duration.between(LocalDateTime.now(),x.getData()).toDays() + " de zile.";
                notificariEvenimente.add(s);
            }

        });
        int i = 0;
        this.idLabel1.setVisible(false);
        this.idLabel2.setVisible(false);
        this.idLabel3.setVisible(false);
        if(!notificariEvenimente.isEmpty())
        {
            this.idLabel1.setText(notificariEvenimente.remove());
            this.idLabel1.setVisible(true);
        }
        if(!notificariEvenimente.isEmpty())
        {
            this.idLabel2.setText(notificariEvenimente.remove());
            this.idLabel2.setVisible(true);
        }
        if(!notificariEvenimente.isEmpty())
        {
            this.idLabel3.setText(notificariEvenimente.remove());
            this.idLabel3.setVisible(true);
        }
        this.idEventTable.getColumns().forEach(x->{
            x.setResizable(false);
            x.setReorderable(false);
        });
        this.loadModel(StreamSupport.stream(evenimenteService.getAll().spliterator(), false)
                .collect(Collectors.toList()));
        this.idEventTable.setItems(model);
        this.idPictureEvent.setCellValueFactory(new PropertyValueFactory<>("imageView"));
        this.idNameEvent.setCellValueFactory(new PropertyValueFactory<>("nume"));
        this.idDataEvent.setCellValueFactory(new PropertyValueFactory<>("stringData"));
        this.idNameEvent.setCellFactory(new Callback<TableColumn<Event, String>, TableCell<Event, String>>() {
            @Override
            public TableCell<Event, String> call(TableColumn<Event, String> param) {
                return new TableCell<Event,String>(){
                    @Override
                    public void updateItem(String item, boolean empty){
                        super.updateItem(item, empty);

                        if(isEmpty())
                        {
                            setText("");
                        }
                        else
                        {

                            setTextFill(Color.BLACK);
                            setFont(Font.font ("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.idDataEvent.setCellFactory(new Callback<TableColumn<Event, String>, TableCell<Event, String>>() {
            @Override
            public TableCell<Event, String> call(TableColumn<Event, String> param) {
                return new TableCell<Event,String>(){
                    @Override
                    public void updateItem(String item, boolean empty){
                        super.updateItem(item, empty);

                        if(isEmpty())
                        {
                            setText("");
                        }
                        else
                        {

                            setTextFill(Color.BLACK);
                            setFont(Font.font ("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });

    }
    private void loadModel(List<Event> list)
    {
        model.setAll(list);
    }
    public void tableClickedEvent(MouseEvent mouseEvent) {
        Event event=this.idEventTable.getSelectionModel().getSelectedItem();
        this.idPozaEvent.setImage(new Image(event.getPicturePath()));
        this.idNumeEventLabel.setText(event.getNume());
        this.idDataEventLabel.setText(event.getStringData());
        this.lastIdEventClicked=event.getId();
        this.idDescriereLabel.setText(event.getDescriere());
        this.idDescriereLabel.setVisible(true);
        this.idPozaEvent.setVisible(true);
        this.idNumeEventLabel.setVisible(true);
        this.idDataEventLabel.setVisible(true);
        if(event.getParticipanti().contains(idUtilizator)){
            this.idSubscribeButton.setVisible(false);
            this.idUnsubscribeButton.setVisible(true);
        }
        else{
            this.idSubscribeButton.setVisible(true);
            this.idUnsubscribeButton.setVisible(false);
        }
    }

    public void myEventsId(MouseEvent mouseEvent) {
        this.loadModel(StreamSupport.stream(evenimenteService.getAll().spliterator(), false)
                .filter(x->x.getParticipanti().contains(idUtilizator))
                .collect(Collectors.toList()));
    }

    public void allEventsClickedEvent(MouseEvent mouseEvent) {
        this.loadModel(StreamSupport.stream(evenimenteService.getAll().spliterator(), false)
                .collect(Collectors.toList()));
    }

    public void subscribeEvent(MouseEvent mouseEvent) {
        Event event=this.evenimenteService.findOne(this.lastIdEventClicked);
        List<Long> list =event.getParticipanti();
        list.add(idUtilizator);
        Event event1=new Event(event.getNume(),event.getData(),list,event.getPicturePath(),event.getImageView(),event.getDescriere());
        event1.setId(event.getId());
        this.evenimenteService.updateEvent(event1);
        this.idSubscribeButton.setVisible(false);
        this.idUnsubscribeButton.setVisible(true);

    }

    public void unsubscribeEvent(MouseEvent mouseEvent) {
        Event event=this.evenimenteService.findOne(this.lastIdEventClicked);
        List<Long> list =event.getParticipanti();
        list.remove(idUtilizator);
        Event event1=new Event(event.getNume(),event.getData(),list,event.getPicturePath(),event.getImageView(),event.getDescriere());
        event1.setId(event.getId());
        this.evenimenteService.updateEvent(event1);
        this.idSubscribeButton.setVisible(true);
        this.idUnsubscribeButton.setVisible(false);
    }

    public void label1Event(MouseEvent mouseEvent) {
        if(notificariEvenimente.isEmpty())
            this.idLabel1.setVisible(false);
        else
        {
            this.idLabel1.setText(notificariEvenimente.remove());
        }
    }

    public void label2Event(MouseEvent mouseEvent) {
        if(notificariEvenimente.isEmpty())
            this.idLabel2.setVisible(false);
        else
        {
            this.idLabel2.setText(notificariEvenimente.remove());
        }
    }

    public void label3Event(MouseEvent mouseEvent) {
        if(notificariEvenimente.isEmpty())
            this.idLabel3.setVisible(false);
        else
        {
            this.idLabel3.setText(notificariEvenimente.remove());
        }
    }
}
