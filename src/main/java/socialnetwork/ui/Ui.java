package socialnetwork.ui;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.CereriPrietenieService;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import java.text.DateFormatSymbols;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Month;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

public class Ui {
    private PrietenieService prietenieService;
    private UtilizatorService utilizatorService;
    private MessageService messageService;
    private CereriPrietenieService cereriPrietenieService;

    public Ui(PrietenieService prietenieService, UtilizatorService utilizatorService, MessageService messageService, CereriPrietenieService cereriPrietenieService) {
        this.prietenieService = prietenieService;
        this.utilizatorService = utilizatorService;
        this.messageService = messageService;
        this.cereriPrietenieService = cereriPrietenieService;
    }

    public void run() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String meniu =
                "1. Adauga utilizator \n" +
                "2. Sterge utilizator \n" +
                "3. Modifica utilizator \n" +
                "4. Adauga prietenie \n" +
                "5. Sterge prietenie \n"+
                "6. Afiseaza numarul de grupuri de prieteni\n" +
                "7. Lista de prieteni a unui utilizator\n" +
                "8. Afiseaza prieteniile\n"+
                "9. Afisare prietenii utilizator\n"+
                "10. Afisare prietenii utilizator luna data\n"+
                "11. Logare\n"+
                "12. Exit\n";
        boolean ok = true;
        this.utilizatorService.reloadFriends();
        while (ok)
        {
            try
            {
                System.out.println(meniu);
                String command = reader.readLine();
                Long id1;
                Long id2;
                String nume;
                String prenume;
                switch (command) {
                    case "1":
                        System.out.println("Dati id");
                        try {
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch(IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        System.out.println("Dati nume");
                        nume = reader.readLine();
                        System.out.println("Dati prenume");
                        prenume = reader.readLine();
                        Utilizator user = new Utilizator(prenume, nume);
                        user.setId(id1);
                        utilizatorService.addUtilizator(user);
                        break;
                    case "2":
                        System.out.println("Dati id");
                        try {
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch(IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        utilizatorService.deleteUtilizator(id1);
                        break;
                    case "3":
                        System.out.println("Dati id");
                        try {
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        System.out.println("Dati nume");
                        nume = reader.readLine();
                        System.out.println("Dati prenume");
                        prenume = reader.readLine();
                        Utilizator utilizator = new Utilizator(prenume, nume);
                        utilizator.setId(id1);
                        utilizatorService.updateUtilizator(utilizator);
                        break;
                    case "4":
                        System.out.println("Dati primul id");
                        try {
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        System.out.println("Dati al doilea id");
                        try {
                            id2 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        Prietenie prietenie = new Prietenie();
                        prietenie.setId(new Tuple<>(id1, id2));
                        prietenieService.addPrietenie(prietenie);
                        break;
                    case "5":
                        System.out.println("Dati primul id");
                        try{
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        System.out.println("Dati al doilea id");
                        try {
                            id2 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        prietenieService.deletePrietenie(new Tuple<>(id1, id2));
                        break;
                    case "6":
                        System.out.println(this.utilizatorService.numberOfGroups().size());
                        break;
                    case"7":
                        System.out.println("Dati id");
                        try{
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        Utilizator u1 = utilizatorService.findOne(id1);
                        u1.getFriends().forEach(System.out::println);
                        break;
                    case"8":
                        this.prietenieService.getAll().forEach(System.out::println);
                        break;
                    case "9":
                        System.out.println("Dati id");
                        try{
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        System.out.println(this.prietenieService.toatePrieteniile(id1));
                        break;
                    case "10":
                        System.out.println("Dati id");
                        try{
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        System.out.println("Dati numarul lunii");
                        int luna;
                        try {
                            luna = Integer.parseInt(reader.readLine());
                            if (luna < 1 || luna > 12)
                                throw new IllegalArgumentException();
                        }

                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("The month number must be a number between 1 and 12");
                            break;
                        }
                        System.out.println("Dati anul");
                        int an = Integer.parseInt(reader.readLine());
                        Month month=Month.of(luna);
                     //   System.out.println(this.prietenieService.toatePrieteniileAn(id1,an));

                        break;
                    case "11":
                        System.out.println("Dati id");
                        try{
                            id1 = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException)
                        {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        if(utilizatorService.existaUtilizator(id1))
                        {
                            UiUtilizator uiUtilizator=new UiUtilizator(messageService,utilizatorService,id1,cereriPrietenieService,prietenieService);
                            uiUtilizator.run();
                        }
                        else
                        {
                            System.out.println("Utilizatorul nu exista!");
                        }

                        break;
                    case "12":
                        ok=false;
                        break;
                    default:
                        System.out.println("Comanda invalida!\n");
                        break;

                }

            }
            catch (ValidationException | RepoException | ServiceException exception){
                System.out.println(exception.getMessage());
            }
        }
    }
}
